package com.example.crud.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(nullable = false)
    private String name;
    private ProductCategory category;
    private boolean availability = true;
    private double price;

    public Product() {}
    public Product(long id, String name, ProductCategory category, boolean availability, double price) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.availability = availability;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ProductCategory getCategory() {
        return category;
    }

    public boolean isAvailability() {
        return availability;
    }

    public double getPrice() {
        return price;
    }
}
