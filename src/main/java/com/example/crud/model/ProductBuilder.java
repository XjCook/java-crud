package com.example.crud.model;

public final class ProductBuilder {
    private long id;
    private String name;
    private ProductCategory category;
    private boolean availability;
    private double price;

    public ProductBuilder() {}

    public ProductBuilder(Product prototype) {
        if (prototype != null) {
            this.id = prototype.getId();
            this.name = prototype.getName();
            this.category = prototype.getCategory();
            this.availability = prototype.isAvailability();
            this.price = prototype.getPrice();
        }
    }

    public ProductBuilder withId(long id) {
        this.id = id;
        return this;
    }

    public ProductBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public ProductBuilder withCategory(ProductCategory category) {
        this.category = category;
        return this;
    }

    public ProductBuilder withAvailability(boolean availability) {
        this.availability = availability;
        return this;
    }

    public ProductBuilder withPrice(double price) {
        this.price = price;
        return this;
    }

    public Product build() {
        return new Product(id, name, category, availability, price);
    }
}
