package com.example.crud.model;

public enum ProductCategory {
    SOFTWARE,
    HARDWARE,
    CUSTOM,
    UNDEFINED
}
