package com.example.crud.controller;

import com.example.crud.model.Product;
import com.example.crud.model.ProductBuilder;
import com.example.crud.repository.ProductRepository;
import com.example.crud.util.ProductUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Controller
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping("/products")
    ResponseEntity<List<Product>> fetchProducts() {
        var products = productRepository.findAll();
        if (products.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/products/available")
    ResponseEntity<List<Product>> fetchAvailableProducts() {
        var products = productRepository.findAll();
        if (products.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(ProductUtil.processProduct(products), HttpStatus.OK);
    }

    @GetMapping("/products/{id}")
    ResponseEntity<Product> fetchProductById(@PathVariable("id") long id) {
        var product = productRepository.findById(id);
        return product.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/products")
    ResponseEntity<Product> addNewProduct(@RequestBody Product product, UriComponentsBuilder uri) {
        var persistedProduct = productRepository.save(product);
        if (ObjectUtils.isEmpty(persistedProduct)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        var headers = new HttpHeaders();
        headers.setLocation(uri.path("/products/{id}").buildAndExpand(product.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @PostMapping("/products/{id}")
    ResponseEntity<Product> updateProductById(@PathVariable("id") long id, @RequestBody Product product) {
        return productRepository.findById(id).map(productDetails -> {
            var updatedProduct = new ProductBuilder(productDetails)
                    .withName(product.getName())
                    .withCategory(product.getCategory() != null ? product.getCategory() : productDetails.getCategory())
                    .withAvailability(product.isAvailability())
                    .withPrice(product.getPrice())
                    .build();
            return new ResponseEntity<>(productRepository.save(updatedProduct), HttpStatus.OK);
        }).orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    @DeleteMapping("/products/{id}")
    ResponseEntity<Void> removeProductById(@PathVariable("id") long id) {
        var product = productRepository.findById(id);
        if (product.isPresent()) {
            productRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @DeleteMapping("/products")
    ResponseEntity<Void> removeProducts() {
        productRepository.deleteAll();
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
