package com.example.crud.util;

import com.example.crud.model.Product;
import com.example.crud.model.ProductBuilder;
import com.example.crud.model.ProductCategory;
import org.springframework.web.util.HtmlUtils;

import java.util.List;

public class ProductUtil {

    private ProductUtil() {}

    public static String escape(String input) {
        return HtmlUtils.htmlEscape(input);
    }

    public static List<Product> processProduct(List<Product> input) {
        return input.stream()
                .filter(Product::isAvailability)
                .map(product -> new ProductBuilder(product)
                        .withName(ProductUtil.escape(product.getName()))
                        .withCategory(product.getCategory() != null ? product.getCategory() : ProductCategory.UNDEFINED)
                        .build())
                .toList();
    }

}
